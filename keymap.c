#include QMK_KEYBOARD_H

#include "keymap_german.h"
#include "config.h"

/*
 *  ____
 * / ___|  ___  ___ _ __ ___   __ _ _ __  _ __
 * \___ \ / _ \/ _ \ '_ ` _ \ / _` | '_ \| '_ \
 *  ___) |  __/  __/ | | | | | (_| | | | | | | |
 * |____/ \___|\___|_| |_| |_|\__,_|_| |_|_| |_|
 *
 * Seemann's keymap for the Ferris Sweep keyboard.
 *
 * OS must be set to German keyboard layout (because of the umlauts).
 *
 */

enum sweep_layers { BASE, SYM, NUM, MOU, NAV };

enum combos {
    COMBO_CAPSW,
    // COMBO_SEMI,
    COMBO_ENTER,
    COMBO_SHIFT_ENTER,
    COMBO_LBRC,
    COMBO_RBRC,
    COMBO_LCBR,
    COMBO_RCBR,
    COMBO_LPRN,
    COMBO_RPRN,
    COMBO_LABK,
    COMBO_RABK,
    // Length
    COMBO_LENGTH
};

uint16_t COMBO_LEN = COMBO_LENGTH;

// combos
const uint16_t PROGMEM caps_word_combo[]   = {LT(SYM, KC_SPC), LT(NAV, KC_TAB), COMBO_END};
// const uint16_t PROGMEM end_sem_combo[]     = {DE_Y, DE_SCLN, COMBO_END};
const uint16_t PROGMEM enter_combo[]       = {LSFT_T(KC_T), RSFT_T(KC_N), COMBO_END};
const uint16_t PROGMEM lbrc_combo[]        = {KC_H, KC_COMM, COMBO_END};
const uint16_t PROGMEM rbrc_combo[]        = {KC_COMM, KC_DOT, COMBO_END};
const uint16_t PROGMEM lcbr_combo[]        = {KC_L, KC_U, COMBO_END};
const uint16_t PROGMEM rcbr_combo[]        = {KC_U, DE_Y, COMBO_END};
const uint16_t PROGMEM lprn_combo[]        = {RSFT_T(KC_N), LALT_T(KC_E), COMBO_END};
const uint16_t PROGMEM rprn_combo[]        = {LALT_T(KC_E), RGUI_T(KC_I), COMBO_END};
const uint16_t PROGMEM labk_combo[]        = {KC_W, KC_F, COMBO_END};
const uint16_t PROGMEM rabk_combo[]        = {KC_F, KC_P, COMBO_END};


combo_t key_combos[] = {
    [COMBO_CAPSW]  = COMBO(caps_word_combo, CW_TOGG),
    // [COMBO_SEMI]   = COMBO_ACTION(end_sem_combo),
    [COMBO_ENTER]  = COMBO(enter_combo, KC_ENT),
    [COMBO_LBRC]   = COMBO(lbrc_combo, DE_LBRC),
    [COMBO_RBRC]   = COMBO(rbrc_combo, DE_RBRC),
    [COMBO_LCBR]   = COMBO(lcbr_combo, DE_LCBR),
    [COMBO_RCBR]   = COMBO(rcbr_combo, DE_RCBR),
    [COMBO_LPRN]   = COMBO(lprn_combo, DE_LPRN),
    [COMBO_RPRN]   = COMBO(rprn_combo, DE_RPRN),
    [COMBO_LABK]   = COMBO(labk_combo, DE_LABK),
    [COMBO_RABK]   = COMBO(rabk_combo, DE_RABK)
};

// void process_combo_event(uint16_t combo_index, bool pressed) {
//     switch (combo_index) {
//         case COMBO_SEMI:
//             if (pressed) {
//                 tap_code16(KC_END);
//                 tap_code16(DE_SCLN);
//             }
//             break;
//     }
// }

const key_override_t delete_key_override = ko_make_basic(MOD_MASK_SHIFT, LT(NAV, KC_BSPC), KC_DEL);

// This globally defines all key overrides to be used
const key_override_t **key_overrides = (const key_override_t *[]){
    &delete_key_override,
    NULL // Null terminate the array of overrides!
};

const uint16_t PROGMEM keymaps[][MATRIX_ROWS][MATRIX_COLS] = {
    /*
     * Second row is hold-key.
     *
     * Layer BASE
     * ,------------------------------------------.     ,--------------------------------------------.
     * |   q   |    w   |   f   |   p   |    b    |     |    j    |    l    |   u   |    y   |   ;   |
     * |       |        |       |       |         |     |         |         |       |        | End-; |
     * |------------------------------------------+     |--------------------------------------------+
     * |   a   |    r   |   s   |   t   |    g    |     |    m    |    n    |   e   |    i   |   o   |
     * | Ctrl  |   Alt  |  Gui  | Shift |         |     |         |  Shift  |  Gui  |   Alt  | Ctrl  |
     * |------------------------------------------+     |--------------------------------------------+
     * |   z   |    x   |   c   |   d   |    v    |     |    k    |    h    |  ,    |    .   |   /   |
     * |       |        |       |       |         |     |         |         |       |        |       |
     * \------------------------------------------+     |--------------------------------------------/
     *                          | Space |  TAB    |     |  Bspc   |   ESC   |
     *                          | _SYM_ | _NAV_   |     |  _NAV_  |  _NUM_  |
     *                          \-----------------/     \-------------------/
     */
    [BASE] = LAYOUT_split_3x5_2(
      KC_Q,         KC_W,         KC_F,         KC_P,         KC_B, /* */ KC_J,         KC_L,         KC_U,         DE_Y, LT(0,DE_SCLN),
      LCTL_T(KC_A), LGUI_T(KC_R), LALT_T(KC_S), LSFT_T(KC_T), KC_G, /* */ KC_M, RSFT_T(KC_N), LALT_T(KC_E), RGUI_T(KC_I), RCTL_T(KC_O),
      DE_Z,         KC_X,         KC_C,         KC_D,         KC_V, /* */ KC_K,         KC_H,      KC_COMM,       KC_DOT,      DE_SLSH,
                                  LT(SYM, KC_SPC), LT(NAV, KC_TAB), /* */ LT(NAV, KC_BSPC), LT(NUM, KC_ESC)
    ),
    /*
     * ,------------------------------------------.     ,--------------------------------------------.
     * |   ^   |    ´   |   '   |   "   |    $    |     |    <    |    >    |   {   |    }   |   %   |
     * |       |        |       |       |         |     |         |         |       |        |       |
     * |------------------------------------------+     |--------------------------------------------+
     * |   #   |    -   |   +   |   =   |    *    |     |    !    |    :    |   (   |    )   |   ?   |
     * |       |        |       |       |         |     |         |         |       |        |       |
     * |------------------------------------------+     |--------------------------------------------+
     * |   /   |    &   |   |   |   _   |         |     |         |    ~    |   [   |    ]   |   @   |
     * |       |        |       |       |         |     |         |         |       |        |       |
     * \------------------------------------------+     |--------------------------------------------/
     *                          |       |         |     |    \    |         |
     *                          |       |         |     |         |  _MOU_  |
     *                          \-----------------/     \-------------------/
     */
    [SYM] = LAYOUT_split_3x5_2(
      DE_CIRC, DE_ACUT, DE_QUOT, DE_DQUO, DE_DLR,  /* */ DE_LABK, DE_RABK, DE_LCBR, DE_RCBR, DE_PERC,
      DE_HASH, DE_MINS, DE_PLUS, DE_EQL,  DE_ASTR, /* */ DE_EXLM, DE_COLN, DE_LPRN, DE_RPRN, DE_QUES,
      DE_SLSH, DE_AMPR, DE_PIPE, DE_UNDS, XXXXXXX, /* */ XXXXXXX, DE_TILD, DE_LBRC, DE_RBRC, DE_AT,
                                 XXXXXXX, XXXXXXX, /* */ DE_BSLS, MO(MOU)
    ),
    /*
     * Layer NUM
     * ,------------------------------------------.     ,--------------------------------------------.
     * |       |    ö   |   ä   |   ü   |         |     |         |    7    |   8   |   9    |       |
     * |       |        |       |       |         |     |         |         |       |        |       |
     * |------------------------------------------+     |--------------------------------------------+
     * |   ß   |   -    |   +   |   =   |   €     |     |    0    |    4    |   5   |   6    | Enter |
     * | Shift |  Ctrl  |  Alt  |  Gui  |         |     |         |         |       |        |       |
     * |------------------------------------------+     |--------------------------------------------+
     * |       |   :    |   ,   |   .   |         |     |         |    1    |   2   |    3   |       |
     * |       |        |       |       |         |     |         |         |       |        |       |
     * \------------------------------------------+     |--------------------------------------------/
     *                          | Space |   TAB   |     | Bspc/Del|         |
     *                          | _MOU_ |         |     |         |         |
     *                          \-----------------/     \-------------------/
     */
    [NUM] = LAYOUT_split_3x5_2(
      XXXXXXX,        DE_ODIA,         DE_ADIA,         DE_UDIA,        XXXXXXX, /* */ XXXXXXX,    LT(0,KC_7), LT(0,KC_8), LT(0,KC_9), XXXXXXX,
      LCTL_T(DE_SS),  LGUI_T(DE_MINS), LALT_T(DE_PLUS), LSFT_T(DE_EQL), DE_EURO, /* */ LT(0,KC_0), LT(0,KC_4), LT(0,KC_5), LT(0,KC_6), KC_ENT,
      XXXXXXX,        DE_COLN,         KC_COMM,         KC_DOT,         XXXXXXX, /* */ XXXXXXX,    LT(0,KC_1), LT(0,KC_2), LT(0,KC_3), XXXXXXX,
                                                       LT(MOU, KC_SPC), _______, /* */ _______, XXXXXXX
    ),
    /*
     * Layer NAV
     * ,------------------------------------------.     ,--------------------------------------------.
     * |   F1  |   F2   |   F3  |   F4  | Vol +   |     |  Home  |         |       |   Ins  |  Del  |
     * |       |        |       |       |         |     |        |         |       |        |       |
     * |------------------------------------------+     |-------------------------------------------+
     * |   F5  |   F6   |   F7  |   F8  | Mute    |     |  Left  |   Down  |   Up  |  Right |  End  |
     * | Shift |  Ctrl  |  Alt  |  Gui  |         |     |        |   Gui   |  Alt  |  Ctrl  | Shift |
     * |------------------------------------------+     |-------------------------------------------+
     * |   F9  |  F10   |  F11  |  F12  | Vol -   |     |  End   |   PgDn  |  PgUp |        |       |
     * |       |        |       |       |         |     |        |         |       |        |       |
     * \------------------------------------------+     |-------------------------------------------/
     *                          |       |   TAB   |     |Bspc/Del|         |
     *                          |       |         |     |        |         |
     *                          \-----------------/     \------------------/
     */
    [NAV] = LAYOUT_split_3x5_2(
      KC_F1,         KC_F2,         KC_F3,         KC_F4,         KC_VOLU, /* */ KC_HOME, KC_INS,          XXXXXXX,       XXXXXXX,         XXXXXXX,
      LCTL_T(KC_F5), LGUI_T(KC_F6), LALT_T(KC_F7), LSFT_T(KC_F8), KC_MUTE, /* */ KC_LEFT, RSFT_T(KC_DOWN), LALT_T(KC_UP), RGUI_T(KC_RGHT), RCTL_T(KC_END),
      KC_F9,         KC_F10,        KC_F11,        KC_F12,        KC_VOLD, /* */ KC_END,  KC_PGDN,         KC_PGUP,       XXXXXXX,         XXXXXXX,
                                                          XXXXXXX, _______, /* */ _______, XXXXXXX
    ),
    /*
     * Layer MOU
     * ,------------------------------------------.     ,--------------------------------------------.
     * |       | Whl le | Mo up | Whl rg| Whl up  |     |        |  Prev   |  Play |  Next  |       |
     * |       |        |       |       |         |     |        |         |       |        |       |
     * |------------------------------------------+     |-------------------------------------------+
     * |       |  Mo le | Mo dn | Mo rg | Whl dn  |     |        |  Btn 1  | Btn 3 | Btn 2  |       |
     * |       |        |       |       |         |     |        |         |       |        |       |
     * |------------------------------------------+     |-------------------------------------------+
     * | Reset |        |       |       |         |     |        |  Acc 0  | Acc 1 | Acc 2  | Reset |
     * |       |        |       |       |         |     |        |         |       |        |       |
     * \------------------------------------------+     |-------------------------------------------/
     *                          |       |         |     |        |         |
     *                          |       |         |     |        |         |
     *                          \-----------------/     \------------------/
     */
    [MOU] = LAYOUT_split_3x5_2(
      XXXXXXX, KC_WH_L, KC_MS_U, KC_WH_R, KC_WH_U, /* */ XXXXXXX, KC_MRWD, KC_MPLY, KC_MFFD, XXXXXXX,
      XXXXXXX, KC_MS_L, KC_MS_D, KC_MS_R, KC_WH_D, /* */ XXXXXXX, KC_BTN1, KC_BTN3, KC_BTN2, XXXXXXX,
      QK_BOOT, XXXXXXX, XXXXXXX, XXXXXXX, XXXXXXX, /* */ XXXXXXX, KC_ACL0, KC_ACL1, KC_ACL2, QK_BOOT,
                                 XXXXXXX, _______, /* */ _______, XXXXXXX)};

bool caps_word_press_user(uint16_t keycode) {
    switch (keycode) {
        // Keycodes that continue Caps Word, with shift applied.
        case KC_A ... KC_Z:
        case KC_MINS:
            add_weak_mods(MOD_BIT(KC_LSFT)); // Apply shift to next key.
            return true;

        // Keycodes that continue Caps Word, without shifting.
        case KC_1 ... KC_0:
        case KC_BSPC:
        case KC_DEL:
        case KC_UNDS:
        case DE_UNDS:
            return true;

        default:
            return false; // Deactivate Caps Word.
    }
}

bool process_record_user(uint16_t keycode, keyrecord_t *record) {
    switch (keycode) {
        case LT(0,KC_0): //sends 0 on tap and Gui+Alt+0 on hold
            if (record->tap.count && record->event.pressed) {
                return true; // Return true for normal processing of tap keycode
                break;
            } else if (record->event.pressed) {
                tap_code16(G(A(KC_0))); // Intercept hold function to send Gui+Alt+0
                return false;
            }
            break;
        case LT(0,KC_1): //sends 1 on tap and Gui+Alt+1 on hold
            if (record->tap.count && record->event.pressed) {
                return true; // Return true for normal processing of tap keycode
                break;
            } else if (record->event.pressed) {
                tap_code16(G(A(KC_1))); // Intercept hold function to send Gui+Alt+1
                return false;
            }
            break;
        case LT(0,KC_2): //sends 2 on tap and Gui+Alt+2 on hold
            if (record->tap.count && record->event.pressed) {
                return true; // Return true for normal processing of tap keycode
                break;
            } else if (record->event.pressed) {
                tap_code16(G(A(KC_2))); // Intercept hold function to send Gui+Alt+2
                return false;
            }
            break;
        case LT(0,KC_3): //sends 3 on tap and Gui+Alt+3 on hold
            if (record->tap.count && record->event.pressed) {
                return true; // Return true for normal processing of tap keycode
                break;
            } else if (record->event.pressed) {
                tap_code16(G(A(KC_3))); // Intercept hold function to send Gui+Alt+3
                return false;
            }
            break;
        case LT(0,KC_4): //sends 4 on tap and Gui+Alt+4 on hold
            if (record->tap.count && record->event.pressed) {
                return true; // Return true for normal processing of tap keycode
                break;
            } else if (record->event.pressed) {
                tap_code16(G(A(KC_4))); // Intercept hold function to send Gui+Alt+4
                return false;
            }
            break;
        case LT(0,KC_5): //sends 5 on tap and Gui+Alt+5 on hold
            if (record->tap.count && record->event.pressed) {
                return true; // Return true for normal processing of tap keycode
                break;
            } else if (record->event.pressed) {
                tap_code16(G(A(KC_5))); // Intercept hold function to send Gui+Alt+5
                return false;
            }
            break;
        case LT(0,KC_6): //sends 6 on tap and Gui+Alt+6 on hold
            if (record->tap.count && record->event.pressed) {
                return true; // Return true for normal processing of tap keycode
                break;
            } else if (record->event.pressed) {
                tap_code16(G(A(KC_6))); // Intercept hold function to send Gui+Alt+6
                return false;
            }
            break;
        case LT(0,KC_7): //sends 7 on tap and Gui+Alt+7 on hold
            if (record->tap.count && record->event.pressed) {
                return true; // Return true for normal processing of tap keycode
                break;
            } else if (record->event.pressed) {
                tap_code16(G(A(KC_7))); // Intercept hold function to send Gui+Alt+7
                return false;
            }
            break;
        case LT(0,KC_8): //sends 8 on tap and Gui+Alt+8 on hold
            if (record->tap.count && record->event.pressed) {
                return true; // Return true for normal processing of tap keycode
                break;
            } else if (record->event.pressed) {
                tap_code16(G(A(KC_8))); // Intercept hold function to send Gui+Alt+8
                return false;
            }
            break;
        case LT(0,KC_9): //sends 9 on tap and Gui+Alt+9 on hold
            if (record->tap.count && record->event.pressed) {
                return true; // Return true for normal processing of tap keycode
                break;
            } else if (record->event.pressed) {
                tap_code16(G(A(KC_9))); // Intercept hold function to send Gui+Alt+9
                return false;
            }
            break;
        case LT(0,DE_SCLN): //sends ; on tap and End+; on hold
            if (record->tap.count && record->event.pressed) {
                tap_code16(DE_SCLN);
                return false; // Return true for normal processing of tap keycode
                break;
            } else if (record->event.pressed) {
                tap_code16(KC_END); // Intercept hold function to send End+Semicolon
                tap_code16(DE_SCLN);
                return false;
            }
            break;
    }
    return true; // this allows for normal processing of key release
}

