
     ____
    / ___|  ___  ___ _ __ ___   __ _ _ __  _ __
    \___ \ / _ \/ _ \ '_ ` _ \ / _` | '_ \| '_ \
     ___) |  __/  __/ | | | | | (_| | | | | | | |
    |____/ \___|\___|_| |_| |_|\__,_|_| |_|_| |_|


Seemann's keymap for the Ferris Sweep keyboard.

OS must be set to German keyboard layout (because of the umlauts).

## Flash

Elite-C

    qmk flash -kb ferris/sweep -km seemann -bl dfu[-split-left|-split-right]

Pro-Micro

    qmk flash -kb ferris/sweep -km seemann -bl avrdude[-split-left|-split-right]


